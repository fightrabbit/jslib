var _ = require('lodash');
var firebase = require('firebase');

/**
 * Wraps the longitude to [-180,180].
 *
 * @param {number} longitude The longitude to wrap.
 * @return {number} longitude The resulting longitude.
 */
var wrapLongitude = function(longitude) {
    if (longitude <= 180 && longitude >= -180) {
      return longitude;
    }
    var adjusted = longitude + 180;
    if (adjusted > 0) {
      return (adjusted % 360) - 180;
    }
    // else
    return 180 - (-adjusted % 360);
}
module.exports.wrapLongitude = wrapLongitude;

var degreesToRadians = function(degrees) {
    if (typeof degrees !== "number" || isNaN(degrees)) {
      throw new Error("Error: degrees must be a number");
    }

    return (degrees * Math.PI / 180);
};
module.exports.degreesToRadians = degreesToRadians;

/**
 * Calculates the distance, in kilometers, between two locations, via the
 * Haversine formula. Note that this is approximate due to the fact that
 * the Earth's radius varies between 6356.752 km and 6378.137 km.
 *
 * @param {Object} location1 The first location given as .latitude and .longitude
 * @param {Object} location2 The second location given as .latitude and .longitude
 * @return {number} The distance, in kilometers, between the inputted locations.
 */
var distance = function(location1, location2) {
    var radius = 6371; // Earth's radius in kilometers
    var latDelta = degreesToRadians(location2.latitude - location1.latitude);
    var lonDelta = degreesToRadians(location2.longitude - location1.longitude);

    var a = (Math.sin(latDelta / 2) * Math.sin(latDelta / 2)) +
            (Math.cos(degreesToRadians(location1.latitude)) * Math.cos(degreesToRadians(location2.latitude)) *
            Math.sin(lonDelta / 2) * Math.sin(lonDelta / 2));

    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));

    return radius * c;
}
module.exports.distance = distance;

/**
 * Calculates the number of degrees a given distance is at a given latitude.
 *
 * @param {number} distance The distance to convert.
 * @param {number} latitude The latitude at which to calculate.
 * @return {number} The number of degrees the distance corresponds to.
 */
var metersToLongitudeDegrees = function(distance, latitude) {
    var EARTH_EQ_RADIUS = 6378137.0;
    // this is a super, fancy magic number that the GeoFire lib can explain (maybe)
    var E2 = 0.00669447819799;
    var EPSILON = 1e-12;
    var radians = degreesToRadians(latitude);
    var num = Math.cos(radians) * EARTH_EQ_RADIUS * Math.PI / 180;
    var denom = 1 / Math.sqrt(1 - E2 * Math.sin(radians) * Math.sin(radians));
    var deltaDeg = num * denom;
    if (deltaDeg < EPSILON) {
      return distance > 0 ? 360 : 0;
    }
    // else
    return Math.min(360, distance / deltaDeg);
}
module.exports.metersToLongitudeDegrees = metersToLongitudeDegrees;

/**
 * Calculates the SW and NE corners of a bounding box around a center point for a given radius;
 *
 * @param {Object} center The center given as .latitude and .longitude
 * @param {number} radius The radius of the box (in kilometers)
 * @return {Object} The SW and NE corners given as .swCorner and .neCorner
 */

var boundingBoxCoordinates = function(center, radius) {
    var KM_PER_DEGREE_LATITUDE = 110.574;
    var latDegrees = radius / KM_PER_DEGREE_LATITUDE;
    var latitudeNorth = Math.min(90, center.latitude + latDegrees);
    var latitudeSouth = Math.max(-90, center.latitude - latDegrees);
    // calculate longitude based on current latitude
    var longDegsNorth = metersToLongitudeDegrees(radius, latitudeNorth);
    var longDegsSouth = metersToLongitudeDegrees(radius, latitudeSouth);
    var longDegs = Math.max(longDegsNorth, longDegsSouth);
    return {
      swCorner: { // bottom-left (SW corner)
        latitude: latitudeSouth,
        longitude: wrapLongitude(center.longitude - longDegs),
      },
      neCorner: { // top-right (NE corner)
        latitude: latitudeNorth,
        longitude: wrapLongitude(center.longitude + longDegs),
      },
    };
}
module.exports.boundingBoxCoordinates = boundingBoxCoordinates;

/**
 * Get locations within a bounding box defined by a center point and distance from from the center point to the side of the box;
 * Adapted from https://stackoverflow.com/questions/46630507/how-to-run-a-geo-nearby-query-with-firestore
 *
 * @param {Object} area an object that represents the bounding box
 *    around a point in which locations should be retrieved
 * @param {Object} area.center an object containing the latitude and
 *    longitude of the center point of the bounding box
 * @param {number} area.center.latitude the latitude of the center point
 * @param {number} area.center.longitude the longitude of the center point
 * @param {number} area.radius (in kilometers) the radius of a circle
 *    that is inscribed in the bounding box;
 *    This could also be described as half of the bounding box's side length.
 * @param {string} collectionRef collection ref to search.
 * @param {string} geoPointProperty name of the object property to look for GeoPoint.
 * @param {object} geoPointcls Geopoint class constructor.
 * @return {Promise} a Promise that fulfills with an array of all the
 *    retrieved locations
 */
module.exports.getLocations = function(area, collectionRef, geoPointProperty, geoPointCls) {
    // calculate the SW and NE corners of the bounding box to query for
    var box = boundingBoxCoordinates(area.center, area.radius);

    // construct the GeoPoints
    var lesserGeopoint = new geoPointCls(box.swCorner.latitude, box.swCorner.longitude);
    var greaterGeopoint = new geoPointCls(box.neCorner.latitude, box.neCorner.longitude);

    if (!collectionRef) {
        return [box, lesserGeopoint, greaterGeopoint];
    }
    // construct the Firestore query
    var query = collectionRef.where(geoPointProperty, '>', lesserGeopoint).where(geoPointProperty, '<', greaterGeopoint);

    // return a Promise that fulfills with the locations
    return query.get()
      .then(function(snapshot) {
        var allLocs = []; // used to hold all the loc data
        snapshot.forEach(function(loc) {
          // get the data
          var data = loc.data();
          // calculate a distance from the center
          data.distanceFromCenter = distance(area.center, data.location);
          // add to the array
          allLocs.push(data);
        });
        return allLocs;
      })
      .catch(function(err) {
        return new Error('Error while retrieving events');
      });
}