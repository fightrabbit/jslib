'use strict';

var expect = require('chai').expect;
var findSlots = require('../index').findSlots;
var getLocations = require('../index').getLocations;

describe('#findSlots', function () {
    var model = [8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20];
    it('should return emtpy array if model < slot size', function () {
        var slots = findSlots(model, [], 14);
        expect(slots.length).to.equal(0);
    });
    it('should return 12 arrays if slotSize = 2', function () {
        var slots = findSlots(model, [], 2);
        expect(slots.length).to.equal(12);
    });
    it('should return 10 arrays if slotSize = 2 and booked=[8,9]', function () {
        var slots = findSlots(model, [8, 9], 2);
        expect(slots.length).to.equal(10);
    });
    it('should return array where 2nd array is [13,14, 15] if slotSize = 2 and booked=[11,12]', function () {
        var slots = findSlots(model, [11, 12], 3);
        expect(slots[1]).to.be.eql([13,14, 15]);
    });
});

describe('#getLocations', () => {
    let area = {
        center: {
            latitude: 0,
            longitude: 0
        },
        radius: 1000
    };
    it('should return box, lesserGP, greaterGP', () => {
        let res = getLocations(area);
        expect(res).to.be('Array');
    });
});